---
sidebar_position: 7
---

# Linting and Formatting

The project uses [eslint](https://eslint.org/) with the
[Prettier plugin](https://github.com/prettier/eslint-plugin-prettier). eslint
handles linting, but eslint rules related to code formatting, they get handled
by prettier. For the most part, out of the box rules provided by the
configurations that are extended are used but there are some tweaks.

forem-ng also has some objects that live in the global scope, e.g. InstantClick.
The eslint globals section of the eslint configuration is what enables these to
be reported as existing when eslint runs.

```javascript
globals: {
  InstantClick: false,
  filterXSS: false,
}
```

[Back to index](https://labspl.github.io/forem-ng/)
