# Workflows

## Preamble

We make extensive use of GitHub automation called **workflows**. They help us to automate repetitive tasks like code/repo cleaning, QA, etc..

## Files location

As per standard set by GitHub, all workflow definition files ( `WDF` for short ) lives in `.github/workflows` dir.

### Current folder structure

```bash
.github/
.github/workflows/
.github/workflows/auto_releaser.yml
.github/workflows/issue_labeler.yml
.github/workflows/issue_triage.yml
```
