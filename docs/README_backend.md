# Backend

* [Authenticating](backend/authentication.md)
* [Auth](backend/authorization.md)
* [Config](backend/configuration.md)
* [DUS](backend/data-update-scripts.md)
* [Database](backend/database.md)
* [Emails](backend/emails.md)
* [Fastly](backend/fastly.md)
* [Internationalization](backend/internationalization.md)
* [Metrics](backend/metrics.md)
* [Notifications](backend/notifications.md)
* [Push](backend/push-notifications.md)
* [roles](backend/roles.md)
* [Jobs](backend/scheduled-jobs.md)
* [Service Objects](backend/service-objects.md)
* [Tracking](backend/tracking.md)

[Back to index](https://labspl.github.io/forem-ng/)