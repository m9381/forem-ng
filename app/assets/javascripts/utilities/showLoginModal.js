function showLoginModal() {
  window.forem-ng.showModal({
    title: 'Log in to continue',
    contentSelector: '#global-signup-modal',
    overlay: true,
  });
}
