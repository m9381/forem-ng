---
sidebar_position: 5
---

# GitPod

If you prefer working on a cloud IDE, you can spin up your own instance of forem-ng
in the cloud, using [GitPod](https://gitpod.io).

1. Fork the forem-ng repository,
2. In the browser address bar enter the URL of your forked repository.  Note: If it's your first time using Gitpod, you will be redirected to their sign in/sign up flow.
3. Gitpod will load your forked version of forem-ng. If it's the first time doing this, it will take several minutes. All the pieces you need for your cloud environment are installing, including Ruby gems and npm packages.
4. Gitpod prompts to install the recommended extensions. It's not required, but it will provide a better editing experience.
5. It will still take a few minutes to prepare the workspace.
6. Two terminal sessions will be running. The one named `Open Site: gp` with the prompt `Awaiting port 3000...`.
This will remain until the web server is started which is running in the other terminal session named `forem-ng Server: bash`.
7. The local forem-ng development instance loads in the Gitpod browser preview window.
8. You can now code, review, or try out the project.

Additional information:

- The forem-ng local development instance ships with one user, the admin account. The email for the user is `admin@forem-ng.local` and the password is `password`.
- Although other methods of authentication are available, only email will work.

To use other forms of authentication, they need to be configured. Visit the [Authentication](../../backend/authentication.md) section to configure social logins.
- Instead of manually building a Gitpod URL and pasting it in the browser's address bar, consider installing the [Gitpod browser extension](https://www.gitpod.io/docs/browser-extension/).

## GitHub CLI

forem-ng’s Gitpod configuration ships with the GitHub CLI. You can create a pull request right from the command line by running the following command: `gh pr create`. 🤯
The first time you use the GitHub CLI in Gitpod for any remote commands, like `gh pr create`, you will need to authenticate to GitHub via the GitHub CLI, i.e. `gh auth login`.
It is recommended to login to GitHub via SSH.
Follow the instructions from the GitHub CLI to finish logging in. The browser login to Github will open in the Gitpod preview browser window and fail to load. Copy the URL from the Gitpod browser preview window and paste it in a browser tab address bar instead to complete the authentication to GitHub.

For more information on how to use the GitHub CLI, see the official [GitHub CLI documentation](https://cli.github.com/manual/index).

[Back to index](https://labspl.github.io/forem-ng/)