# Design guide

* [Branding](design-guide/branding.md)
* [Language](design-guide/language.md)
* [Theming](design-guide/theming.md)

[Back to index](https://labspl.github.io/forem-ng/)