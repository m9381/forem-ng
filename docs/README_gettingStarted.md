# Getting started

* [Branching](getting-started/branching.md)
* [Commiting](getting-started/commiting.md)
* [PR](getting-started/pull-request.md)
* [Start-app](getting-started/start-app.md)
* [Syncing](getting-started/syncing.md)
* [Workflows](getting-started/workflow.md)
* [Organizations](getting-started/orgs.md)
* [Open resources](getting-started/open-res/README.md)

## Installation

* [Containers](getting-started/installation/containers.md)
* [GitPod](getting-started/installation/gitpod.md)
* [Image Proxy](getting-started/installation/imgproxy.md)
* [Linux](getting-started/installation/linux.md)
* [Mac](getting-started/installation/mac.md)
* [Other Systems](getting-started/installation/others.md)
* [Postgresql](getting-started/installation/postgresql.md)
* [Vault](getting-started/installation/vault.md)
* [Windows](getting-started/installation/windows.md)

[Back to index](https://labspl.github.io/forem-ng/)