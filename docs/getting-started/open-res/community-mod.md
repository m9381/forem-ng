# Community moderation

*This forem-ng name* gives trusted users the ability to take certain privileged actions on the platform. For instance, they can help promote valuable posts, gently reduce the visibility of low-quality posts (spammy, overly promotional, or unhelpful), and quickly flag rule-breaking content to our admin team.

To sum up a few trusted user abilities, they can:

* Use quick reactions to help moderate content
* Seamlessly flag rule-breaking behavior to admins
* Rate the experience level of an article
* Access the Mod Center

***To become a trusted user or [tag moderator](/tag-moderation), please email << YOUR_EMAIL>>**

## How to Moderate Articles

We have two ways to moderate **articles**:

* Mod Panel,
* Mod Center.

**Please note**: you will need to be assigned Trusted User or Tag Moderator status **and** signed in to carry out all the following actions.

## Using the quick reactions to moderate content

Trusted users are able to make use of the thumbsup, thumbsdown, and “Flag to Admins” buttons. Here's how each reaction works:

* **Thumbsup (👍)**: a quick way to "push content up" so that it's more likely to be seen on the site (i.e. articles with high-quality content that embody the values and spirit of the community).
* ***Thumbsdown (👎)**: a quick way to "push content down" so that it's less likely to be seen on the site (e.g. articles that only have a teaser paragraph and send the reader to an external link for the rest).
* **Flag to Admins (🧐)**: a quick way to flag content that violates the [Code of Conduct](https://github.com/labspl/coding-standards/blob/main/ReposStandards/coc.md) so the admin team is notified and can take action.


*Note: It is not possible to choose the "thumbsup" reaction together with either the "thumbsdown" or "Flag to Admins" reaction.*

## Examples of when to use 👎 vs. 🧐

Knowing when to use a “thumbsdown” vs. a “Flag to Admins” reaction can be confusing. The following guidelines should help.

## Use 👎 for articles
* purely linking to external content (e.g. "read more here")
* advertising a product or service in a distasteful way (e.g. stealthy or dishonest marketing tactics)
* factually incorrect

## Use 🧐 for articles…
* clearly spam (completely off-topic, etc.)
* do not abide by our [Code of Conduct](https://github.com/labspl/coding-standards/blob/main/ReposStandards/coc.md) (i.e. posts that are negative and exclusive)

*Note: “Thumbsdown” and “Flag to Admins” reactions are distinct from one another, but using the “Flag to Admins” reaction on an article will essentially thumbsdown the post too, so there is no need to combine them.*

## Flagging a problematic account / person

If you notice a particularly harmful account and want to quickly flag all of this account’s content, then you can use the following methods.

From the ModPanel, click “Flag user”:

*Note: By clicking “Flag user”, you are bulk reacting with the 🧐 on all of this user's posts. There's no need to flag each individual post by the user once you've flagged the user.*

---

## How to Moderate Comments

To moderate comments, click 'moderate' from the ellipses on the comment, or append `/mod` to the comment permalink.

---

## How to Rate the Experience Level of a Post

Using the 1-5 scale (1: novice; 5: expert), rate what you believe a post's experience level to be. While this is somewhat subjective, just use your best judgment taking into account any bias you may have based on your own knowledge of the topic. This will help us bring more relevant posts to each individual's feed.