---
sidebar_position: 10
---

# Submodules

We make use of `git` feature called `submodules`.

Currently, there is only one submodule:

* [Disposable email domains](https://github.com/disposable-email-domains/disposable-email-domains)