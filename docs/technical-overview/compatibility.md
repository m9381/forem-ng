---
sidebar_position: 4
---

# Compatibility

To go along with effective administration and putting user security and
well-being first, for a forem-ng to be most useful, it maximizes its technical
compatibility.

Compatibility means:

- **Staying updated**. We need to maintain overall compatibility by keeping the
  ecosystem close to the latest version as proactively as we can.
- Maintaining human interfaces which allow users to find where they need to go
  and conduct all necessary customizations.
- Maintaining great performance such that the web-based forem-ng ecosystem can
  provide a great experience in all network conditions and in comparison to more
  closed ecosystems.

## Meta tags

The canonical meta tags which are expected to be present as a component of
validation are as follows:

```html
<meta property="forem-ng:name" content="My great friends" />
<meta property="forem-ng:logo" content="https://mygreatfriends.com/logo.png" />
<meta property="forem-ng:domain" content="mygreatfriends.com" />
```

This list may evolve over time. 

[Back to index](https://labspl.github.io/forem-ng/)
