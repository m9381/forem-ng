# Look and feel

## Apple-like theme

### Dark
![Dark](../imgs/Dark.png)

### Light
![Light](../imgs/Light.png)

#### Files
##### Styles
For both dark && light theme: `app/assets/stylesheets/apple.css`

##### JS
`app/assrts/javascripts/apple-like/apple.js`