# How to report inappropriate content in *Your forem-ng Name* community

If you have any reason to believe that a user is abusing their posting or commenting privileges to share inappropriate content, whether that is content that violates the community [Code of Conduct](https://github.com/labspl/coding-standards/blob/main/ReposStandards/coc.md). We encourage you to report this for consideration by our Admin team. 

Whilst you can use the universal [Reporting Form](https://web_addr/report-abuse) to report any URL, we also have some shortcuts to make it easier for you to report content on the fly:

* From the Reactions Sidebar on any post, you'll see a More button `...` below the Heart 💜, Unicorn 🦄, and Save to Reading List 🔖 buttons. Inside this menu, you'll find the Report Abuse button that will take you straight to the [Reporting Form](https://web_addr/report-abuse) with the Post URL pre-filled:
* Another shortcut is available at the bottom of every Post Discussion:
* Meanwhile, if you'd like to report an inappropriate comment, you can do so from the same More button `...` in the top-right corner of any Comment:

If you're logged into account, your username will be attached to the report in case an Admin has any questions to ask you. If you're not logged in, your report will be anonymous. If you'd like to make your report anonymously, remember to log out before opening the Report Form.

## What happens when you Report content?

If you were logged in, you will receive an email acknowledging you've reported content. 

Reports appear in the Admin Portal for moderation. Admins can review reports and take any action that they find appropriate in line with the community Code of Conduct and/or Terms of Use. If you submitted a report and feel like timely action hasn't been taken, or disagree with the outcome of the review, please email us at *help@yourforem.name* 

## Feedback

We hope this information helps you feel safer whilst participating in this community, and empowered to take action at any time. 

If you'd ever like to request changes to the feedback procedure, the forem-ng team would love to hear from you. They invite you to [request changes or report bugs here](https://github.com/labspl/forem-ng/issues/new/choose).