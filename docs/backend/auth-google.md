---
sidebar_position: 6
---

# Google OAuth2 Authentication

forem-ng allows you to authenticate using Google OAuth2. To use this authentication method
in local development, you will need to set up Google client application and retrieve its
keys. Then you'll need to provide these keys to the Rails application.

## Google Admin Console.

1. [Click this link to create a new OAuth application in your Google console](https://console.cloud.google.com/projectcreate) -
   you will be redirected to sign in to Google account if you have not already.
2. Go to **OAuth Consent Screen** section. Set the **Publishing Status** to **Testing**
3. Add your test user email to the **Test Users** section
4. Go to the **Credentials** section and at the top of the screen click **Create Credentials** -> **OAuth Client ID**=
5. Select **Web Application** as the **Application Type**, and name it
6. Under **Authorized Redirect URIs** add the URI `http://localhost:3000/users/auth/google_oauth2/callback`. Replace the port `3000` 
   if you  run forem-ng on another port.
7. On the resulting screen you'll find your app's **Client ID** and **Client Secret**. Make a note of them.
8. Click  **OAuth Consent Screen** in the sidebar, then click **Edit App** next to your app's name
9. Fill out the information on the resulting **Edit App Registration** screen, then click **Next Step**
10. On the next screen, **Scopes**, click **Add or Remove Scopes**
11. Click the checkboxes next to the first two unlabeled scopes in the **Updated Selected Scopes** section
12. Complete the rest of the app registration screens

## Your forem-ng

13. Enable the Google Authentication provider in the admin dashboard, entering the **Client ID** and **Client Key** from step 7.
14. Done.

[Back to index](https://labspl.github.io/forem-ng/)
