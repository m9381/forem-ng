# Welcome

**From 7-03-2022 forem-ng is deprecated**

No further work will be done on this project.


Please select what you want to explore

* [Admin](README_admin.md)
* [Backend](README_backend.md)
* [Contributing Guide](README_contribs.md)
* [Creators guide](README_creators.md)
* [Design guide](README_dg.md)
* [Getting started](README_gettingStarted.md)
* [Frontend](README_frontend.md)
* [Maintainers](README_maintainers.md)
* [Technical overview](README_tech-overview.md)

* [API](API/README_api.md)

* [Project Licensing](licensing.md)
* [Troubles?](troubleshooting.md)
