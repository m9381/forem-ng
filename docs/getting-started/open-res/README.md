# Open resources

## Whats this?
Included in this folder are **templates** for admins / devs to customize and use in their own setups.

## License
As all of the forem-ng, these are also AGPLv3 licensed.

* [About](about.md)
* [Community moderation](community-mod.md)
* [How to ask for help](how-to-ask.md)
* [How to report abuse](how-to-report.md)
* [Making your site work for you](making-your-site-work-for-you.md)