---
sidebar_position: 2
---

# Authentication

Authentication is handled by [Devise](https://github.com/plataformatec/devise)
and [OmniAuth](https://github.com/omniauth/omniauth).

On forem-ng you can authenticate through Facebook, GitHub, Twitter or Apple. Please check out
the respective guides on how to authenticate:

- [forem-ng authentication](auth-forem-ng)
- [Apple authentication](auth-apple)
- [Facebook authentication](auth-facebook)
- [Github authentication](auth-github)
- [Google authentication](auth-google)
- [Twitter authentication](auth-twitter)

We may add other authentication providers in the future. Please check back
again, or search
[our GitHub repository's issues.](https://github.com/labspl/forem-ng/issues)

[Back to index](https://labspl.github.io/forem-ng/)
