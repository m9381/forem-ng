---
sidebar_position: 2
---

# Admin Search

For admin views that need to take advantage of searching and filtering, we've
chosen to use [Ransack][ransack].

Ransack is a Ruby gem that makes searching relatively painless. It has excellent
documentation, but if you're looking for an example of how it's being used on
forem-ng, we've implemented it to help searching and sorting user reports.

The view responsible for managing user reports can be found at
`/admin/moderation/reports`.

For forem-ng, Ransack is being used exclusively in admin.

[Back to index](https://labspl.github.io/forem-ng/)