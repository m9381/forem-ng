# Organizations

## Snippets

### List

**Request**

```curl
curl https://web-addr/api/organizations/ecor
```

**Response**

```curl
{
  "type_of": "organization",
  "username": "ecorp",
  "name": "E Corp",
  "summary": "Together we can change the world, with E Corp",
  "twitter_username": "ecorp",
  "github_username": "ecorp",
  "url": "https://ecorp.internet",
  "location": "New York",
  "joined_at": "2019-10-24T13:41:29Z",
  "tech_stack": "Ruby",
  "tag_line": null,
  "story": null,
  "profile_image": "https://res.cloudinary.com/...jpeg"
}
```

[Back to index](https://labspl.github.io/forem-ng/)