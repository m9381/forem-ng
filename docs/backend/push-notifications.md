---
sidebar_position: 18
---

# Push Notifications

**Delivery**
forem-ng instances rely on [Rpush](https://github.com/rpush/rpush) to deliver push notifications. This decision was heavily influenced by the desire to provide as much flexibility as possible to Creators. In order to do this, forem-ng instances can register and configure a `ConsumerApp`.

These consumer apps represent mobile applications that users use to browse and consume content on a forem-ng. Authenticated users of a specific forem-ng instance can register a `Device` associated to a `ConsumerApp`. With these pieces we are able to deliver push notifications to users devices.

The `ConsumerApp` configuration page can be found at `/admin/apps/consumer_apps`. The official forem-ng apps are supported by default and require their secret credential to be provided via ENV variable.

## Rpush Implementation

We use Rpush's `rpush-redis` implementation for the reasons why), hence all `Rpush` models are persisted in Redis.

More details about how this works [here](https://github.com/rpush/rpush/wiki/Using-Redis).

[Back to index](https://labspl.github.io/forem-ng/)
