# Comments

## Snippets

### Get comments

This endpoint allows the client to retrieve all comments belonging to an article or podcast episode as threaded conversations.

It will return the all top level comments with their nested comments as threads. See the format specification for further details.

**Request**

```curl
curl https://web_addr/api/comments?a_id=270180
```

**Response**

```curl
[
  {
    "type_of": "comment",
    "id_code": "m3m0",
    "created_at": "2020-07-01T17:59:43Z",
    "body_html": "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body>\n<p>...</p>\n\n</body></html>\n",
    "user": {
      "name": "Heriberto Morissette",
      "username": "heriberto_morissette",
      "twitter_username": null,
      "github_username": null,
      "website_url": null,
      "profile_image": "https://res.cloudinary.com/...jpeg",
      "profile_image_90": "https://res.cloudinary.com/...jpeg"
    },
    "children": []
  },
  {
    "type_of": "comment",
    "id_code": "m357",
    "created_at": "2020-07-02T17:19:40Z",
    "body_html": "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body>\n<p>...</p>\n\n<p>...</p>\n\n</body></html>\n",
    "user": {
      "name": "Dario Waelchi",
      "username": "dario waelchi",
      "twitter_username": null,
      "github_username": null,
      "website_url": null,
      "profile_image": "https://res.cloudinary.com/...png",
      "profile_image_90": "https://res.cloudinary.com/...png"
    },
    "children": [
      {
        "type_of": "comment",
        "id_code": "m35m",
        "created_at": "2020-08-01T11:59:40Z",
        "body_html": "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n<html><body>\n\n<p>...</p>\n\n</body></html>\n",
        "user": {
          "name": "rhymes",
          "username": "rhymes",
          "twitter_username": null,
          "github_username": null,
          "website_url": null,
          "profile_image": "https://res.cloudinary.com/...jpeg",
          "profile_image_90": "https://res.cloudinary.com/practicaldev/image/fetch/s--SC90PuMi--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/2693/146201.jpeg"
        },
        "children": []
      }
    ]
  }
]
```

[Back to index](https://labspl.github.io/forem-ng/)