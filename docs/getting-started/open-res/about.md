> *What this community is here for, and who it is for. Why we are making a contribution to the community we serve/what needs we meet or pain points we soothe are.*

We are powered by forem-ng, an [open source](https://github.com/labspl/forem-ng) application, meaning you can inspect every little detail of the code, or chip in yourself! forem-ng empowers community by fostering thoughtful connection.

## Leadership

*Who runs your company, and what drives them?*

## Team

> *Consider adding a photo of your team.*

Our team is located here/there/all over the world. *What is your team's mission or a unifying goal?*