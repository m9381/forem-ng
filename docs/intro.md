---
sidebar_position: 1
slug: /
---

# Introduction 👋
## Welcome to forem-ng's developer documentation 🌱

On this site you'll find instructions to setup a [local instance of
forem-ng](./getting-started/installation/mac), documentation on the [architecture of
forem-ng](./technical-overview/architecture/), [how to contribute](./contributing-guide/docs), and many other useful
documents.

This documentation site is the product of a number of volunteer contributors
working alongside the forem-ng Core Team, special thanks to all those who have
contributed to the documentation.

:::important
This is no longer forem-ng
:::