---
title: forem-ng Authentication
sidebar_position: 4
---

# forem-ng Authentication

forem-ng allows you to authenticate using the [forem-ng Passport](https://passport.forem-ng.com) service. Enabling this authentication provider is highly encouraged in order to benefit from the many forem-ng Ecosystem integrations (i.e. mobile apps).

Better integration with the `/admin` dashboard is currently under development but in the mean time you will be able to enable the forem-ng Authentication provider with the following steps:

1. Create an account in https://passport.forem-ng.com
2. Send an email to [hello+passport@forem-ng.com](mailto:hello+passport@forem-ng.com) requesting access to the Creator Tools
3. Use the [forem-ng Creator Tools](https://passport.forem-ng.com/oauth/applications) to add your site to get the `Key` and `Secret` credentials.
4. Enable the forem-ng Authentication provider in the admin dashboard


![forem-ng Creator Tools](https://user-images.githubusercontent.com/6045239/139337919-348a7c1d-e1ab-4ab9-a1c6-492ec475cfa0.png)

**forem-ng Creator Tools**

![New forem-ng Credentials Form](https://user-images.githubusercontent.com/6045239/139337938-8b465cb0-d7d4-4a8a-bfd9-284bff5e00fb.png)

**Your forem-ng instance domain**

![forem-ng Passport credentials](https://user-images.githubusercontent.com/6045239/139337944-79b46cd3-642a-4315-8c65-a5c9e2584bc9.png)

**Successful request for credentials (also able to manage it)**

[Back to index](https://labspl.github.io/forem-ng/)