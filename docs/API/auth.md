# Authentication

## API Key authentication.

Authentication for some endpoints, like write operations on the Articles API require a API key.
All authenticated endpoints are CORS disabled, the API key is intended for non-browser scripts.

## Getting an API key

To obtain one, please follow these steps:

* visit [/settings/account](*) path within AdminPanel script instance,
* in the "API Keys" section create a new key by adding a description and clicking on "Generate API Key",


[Back to index](https://labspl.github.io/forem-ng/)