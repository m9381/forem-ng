---
sidebar_position: 19
---

# Tracking

**Important info**

On 26.01.2022, we ( labspl ) decided to **entirely remove every possible usage of any tracking method.** from forem-ng.

[Back to index](https://labspl.github.io/forem-ng/)