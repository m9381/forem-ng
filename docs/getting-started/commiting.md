---
sidebar_position: 4
---

# Committing

**Committing and pre-commit hooks**

## Commit messages

We encourage people to write
[meaningful commit messages](https://chris.beams.io/posts/git-commit/).

## Style guide

This project follows
[thoughtbot's Ruby Style Guide](https://github.com/thoughtbot/guides/blob/main/ruby/.rubocop.yml),
using [Rubocop](https://github.com/bbatsov/rubocop) along with
[Rubocop-Rspec](https://github.com/backus/rubocop-rspec) as the code analyzer.
If you have Rubocop installed with your text editor of choice, you should be up
and running.

For the frontend, [ESLint](https://eslint.org) and
[prettier](https://github.com/prettier/prettier) are used. ESLint's recommended
rules along with Preact's recommended rules are used for code-quality.
Formatting is handled by prettier. If you have ESLint installed with your text
editor of choice, you should be up and running.

[Back to index](https://labspl.github.io/forem-ng/)
