| Name       | Description |
|------------|---|
| CircleCI   | [![CircleCI](https://circleci.com/gh/labspl/forem-ng/tree/develop.svg?style=svg)](https://circleci.com/gh/labspl/forem-ng/tree/develop)

## What happened?

Long story short: Original creators turned out to be **untrustworthy and destructive**, so we ( members of labspl ) decided to part ways.
We forked, asked GitHub Support to detach fork, and started work.

## What is forem-ng?

forem-ng is open source software for building communities.

## Core team

- [labspl](https://github.com/labspl)
- [Darshan4114](https://github.com/Darshan4114)

New contributors welcome :)

## Contributing, Community, Installation, Getting started and other guides

Please refer to [our documentation](https://labspl.github.io/forem-ng/)

## License

Please see [license file](https://github.com/labspl/forem-ng/blob/develop/LICENSE.md) for full license text.
