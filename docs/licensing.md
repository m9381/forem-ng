---
sidebar_position: 12
---

# Licensing

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. Please see the
[LICENSE](https://github.com/labspl/forem-ng/blob/develop/LICENSE.md) file in our
repository for the full text.

## Where can I learn more about the AGPL-3 license?

Please refer to the official
“[Why the Affero GPL](https://www.gnu.org/licenses/why-affero-gpl.html)” and the
[FAQ page](https://www.gnu.org/licenses/gpl-faq.html) on
[GNU.org](https://www.gnu.org) for full details regarding this license

[Back to index](README.md)