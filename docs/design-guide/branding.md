---
sidebar_position: 1
---

# Branding Guidelines

## forem-ng Theme

forem-ng's design inspiration is meant to be a throwback. In personality and
design, it is ATARI inspired. Magazines like PAPER are also an inspiration in
terms of being a
[bold contrast with some more indy](https://www.google.com/search?biw=1440&bih=780&tbm=isch&sa=1&ei=KSN8W5WVLoy55gLI77TgBA&q=paper+magazine+cover&oq=paper+magazine+cover).
## Branding Rules

- We **dont force** usage of specific fonts upon anyone, use whatever font you like,
- Black and white is our default logo, but the logo can be drawn in any color,
- We use bold shadows (no gradient) for boxes,
- Refer to the `variables.scss`
  [file in our main repo](https://github.com/labspl/forem-ng/blob/develop/app/assets/stylesheets/variables.scss) to reference commonly-used colors and fonts.

## Design License Info

If not sepcefied otherwise, all of our designs, design patterns are licensed under [GNU AGPL](https://github.com/labspl/forem-ng/blob/develop/LICENSE.md)

[Back to index](https://labspl.github.io/forem-ng/)